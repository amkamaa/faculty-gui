package entities;

import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector3f;

public class Camera {
  private Vector3f position = new Vector3f(0, 0, 0);
  private float pitch;
  private float yaw;
  private float roll;

  public Camera() {
  }

  public void move() {
    if (Keyboard.isKeyDown(Keyboard.KEY_NUMPAD8)) {
      position.y += 0.2f;
    }
    if (Keyboard.isKeyDown(Keyboard.KEY_NUMPAD4)) {
      position.x -= 0.2f;
    }
    if (Keyboard.isKeyDown(Keyboard.KEY_NUMPAD6)) {
      position.x += 0.2f;
    }
    if (Keyboard.isKeyDown(Keyboard.KEY_NUMPAD2)) {
      position.y -= 0.2f;
    }
    if (Keyboard.isKeyDown(Keyboard.KEY_NUMPAD7)) {
      position.z -= 0.2f;
    }
    if (Keyboard.isKeyDown(Keyboard.KEY_NUMPAD1)) {
      position.z += 0.2f;
    }
  }

  public Vector3f getPosition() {
    return position;
  }

  public float getPitch() {
    return pitch;
  }

  public float getYaw() {
    return yaw;
  }

  public float getRoll() {
    return roll;
  }
}
